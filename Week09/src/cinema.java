import javax.persistence.*;

/** This Data Object class corresponds with customer table
 *  in database. */
@Entity
@Table(name = "movie")

public class cinema {

    /** id is an identity type field in the database and the primary key */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "movie_id")
    private int movie_id;

    @Column(name = "title")
    private String title;

    @Column(name = "overview")
    private String overview;

    @Column(name = "tagline")
    private String tagline;


    public int getMovie_Id() {
        return movie_id;
    }

    public void setMovie_Id(int movie_id) {
        this.movie_id = movie_id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }


    public String getOverview() {
        return overview;
    }

    public void setOverview(String overview) {
        this.overview = overview;
    }

    public String getTagline() {
        return tagline;
    }

    public void setTagLine(String tagLine) {
        this.tagline = tagline;
    }

    public String toString() {
        return Integer.toString(movie_id) + " \n\n " + title + " \n\n " + overview + " \n\n " + tagline;
    }
}

