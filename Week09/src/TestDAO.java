import org.hibernate.Session;
import org.hibernate.SessionFactory;
import java.util.*;

/* TestDAO implemented using a singleton pattern
 *  Used to get customer data from my MYSQL database*/
public class TestDAO {

    SessionFactory factory = null;
    Session session = null;

    private static TestDAO single_instance = null;

    private TestDAO()
    {
        factory = Hibernate.getSessionFactory();
    }

    /* get an instance of the class. */
    public static TestDAO getInstance()
    {
        if (single_instance == null) {
            single_instance = new TestDAO();
        }

        return single_instance;
    }

    /*  get more than one movie from cinema database
     *  Uses the OpenSession construct rather than the
     *  getCurrentSession method so that I control the
     *  session. */
    public List<cinema> getMovie() {

        try {
            session = factory.openSession();
            session.getTransaction().begin();
            String sql = " https://bitbucket.org/mizuki714/homework/src/master/Week09/src/RunHibernate.cinema";
            List<cinema> cs = (List<cinema>)session.createQuery(sql).getResultList();
            session.getTransaction().commit();
            return cs;

        } catch (Exception e) {
            e.printStackTrace();
            // Rollback in case of an error occurred.
            session.getTransaction().rollback();
            return null;
        } finally {
            session.close();
        }

    }

    /* get a movie from database */
    public cinema getMovie(int movie_id) {

        try {
            session = factory.openSession();
            session.getTransaction().begin();
            String sql = "RunHibernate.cinema from  where movie_id=" + Integer.toString(movie_id);
            cinema m = (cinema)session.createQuery(sql).getSingleResult();
            session.getTransaction().commit();
            return m;

        } catch (Exception e) {
            e.printStackTrace();
            // Rollback in case of an error occurred.
            session.getTransaction().rollback();
            return null;
        } finally {
            session.close();
        }
    }

}