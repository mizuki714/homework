package com.example.servletweek07;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet(name = "Servlet2", value = "/Servlet2", urlPatterns = {"/Servlet2"})
public class Servlet2 extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType ("Text/html");
        PrintWriter out = response.getWriter();
        out.println("This is the get method.");
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        PrintWriter out = response.getWriter();
        response.setContentType("text/html");
        out.println("<html><head></head><body>");
        String username = request.getParameter("username");
        String password = request.getParameter("password");
        out.println("<h1> The login Information</h1>");
        out.println("<p> Username: " + username + "</p>");
        out.println("<p> Password: " + password + "</p>");
        out.println("</body></html>");
    }
}
