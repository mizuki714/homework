import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

class Executeable {
    public Executeable() {
    }

    public static void Main(String[] args) {
        ExecutorService myService = Executors.newFixedThreadPool(10);
        Racing racer1 = new Racing("Flying Rubber", 25, 200);
        Racing racer2 = new Racing("Rocket Fly", 10, 500);
        Racing racer3 = new Racing("Thunder Prince", 25, 250);
        Racing racer4 = new Racing("Burning Baroness", 18, 100);
        Racing racer5 = new Racing("Speedy Devil", 1, 50);
        Racing racer6 = new Racing("Lightening King", 20, 160);
        Racing racer7 = new Racing("Bullet", 22, 54);
        Racing racer8 = new Racing("Lightspeed", 15, 25);
        Racing racer9 = new Racing("Speed Queen", 5, 10);
        Racing racer10 = new Racing("Turbo", 2, 5);
        Racing racer11 = new Racing("Pokey", 1, 50);
        myService.execute(racer1);
        myService.execute(racer2);
        myService.execute(racer3);
        myService.execute(racer4);
        myService.execute(racer5);
        myService.execute(racer6);
        myService.execute(racer7);
        myService.execute(racer8);
        myService.execute(racer9);
        myService.execute(racer10);
        myService.execute(racer11);
        myService.shutdown();
    }
}
