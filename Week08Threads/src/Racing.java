import java.util.Random;

public class Racing implements Runnable {
    private String name;
    private int number;
    private int rTime;
    private int rand;

    public Racing(String name, int number, int rTime) {
        this.name = name;
        this.number = number;
        this.rTime = rTime;
        Random random = new Random();
        this.rand = random.nextInt(600);
    }

    public void race() {
        System.out.println("\n\nThe Race is starting : Racer Name =" + this.name + "\n Racer Number = " + this.number + "\n Race Time = " + this.rTime + " seconds. \nRand Num = " + this.rand + "\n\n");

        for(int count = 1; count < this.rand; ++count) {
            if (count % this.number == 0) {
                System.out.print(this.name + " is Racing. ");

                try {
                    Thread.sleep((long)this.rTime);
                } catch (InterruptedException var3) {
                    System.err.println(var3.toString());
                }
            }
        }

        System.out.println("\n\n" + this.name + " is done with the race.\n\n");
    }

    public void run() {
    }
}
