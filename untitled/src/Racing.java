import java.awt.event.ActionEvent;
import java.util.Random;

public class Racing implements Runnable
{

    private String name;
    private int number;
    private int rTime;
    private int rand;

    public Racing(String name, int number, int rTime) {

        this.name = name;
        this.number = number;
        this.rTime = rTime;

        Random random = new Random();
        this.rand = random.nextInt(600);
    }

    public void race() {
        System.out.println("\n\nThe Race is starting : Racer Name =" + name + "\n Racer Number = "
                + number + "\n\n Race Time = " + rTime + " seconds. \nRand Num = " + rand + "\n\n");
        for (int count = 1; count < rand; count++) {
            if (count % number == 0) {
                System.out.print(name + " is Racing. \n\n");
                try {
                    Thread.sleep(rTime);
                } catch (InterruptedException e) {
                    System.err.println(e.toString());
                }
            }
        }
        System.out.println("\n\n\n" + name + " is done with the race.\n\n\n");
    }


    @Override
    public void run() {
        System.exit(0);
    }
}
